
import tkinter as tk
LARGE_FONT = ("Verdana", 20)
MEDIUM_FONT = ("Verdana", 10)

WIDTH   = 700
HEIGHT  = 700 

class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent )
        label = tk.Label(self, text="Minesweeper", font=LARGE_FONT)
        label.configure(anchor="center")
        label.pack()

        intro = """Inspired by one of my favourite childhood games MineSweeper, I decided to
        code my own MineSweeper game to better learn Python programming language, specialy Python module for creating
        graphical user interfaces Tkinter. If you like this version please feel free to inspect source code
        on my gitlab repository to see how I handled some stuff.
        Have fun!
        Frootek
        """

        introLabel = tk.Label(self, text=intro, font=MEDIUM_FONT)
        introLabel.configure(anchor="center")
        introLabel.pack()




        btn = tk.Button(self, text="Start the game!", width = 20, height=5, bg="blue",fg="white",
        command = lambda: controller.show_frame("MineSweeper") )
        btn.pack()
