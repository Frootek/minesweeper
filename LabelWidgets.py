import tkinter as tk


class Wins(tk.Label):
    def __init__(self, master=None):
        super().__init__(master, text="Wins: 000", bg="black", fg="red")
        self.pack(padx=5, pady=10, expand=True, side=tk.LEFT)
        self.wins = 0

    def update(self):
        self.wins += 1 
        super().config(text = "Wins: {0:03}".format(self.wins))



class CenterBtn(tk.Button):
    def __init__(self, master, parent):
        super().__init__(master,text="Reset score", bg="red", fg="white",
        command = lambda : self.reset_score())
        self.pack(padx=5, pady=10, expand=True, side=tk.LEFT)
        self.parent = parent
    
    def reset_score(self):
        self.parent.wins.wins = -1
        self.parent.wins.update()
        self.parent.loses.loses = -1
        self.parent.loses.update()




class Loses(tk.Label):
    def __init__(self, master=None):
        super().__init__(master, text="Loses: 000", bg="black", fg="red")
        self.pack(padx=5, pady=10, expand=True, side=tk.LEFT)
        self.loses = 0

    def update(self):
        self.loses += 1 
        super().config(text = "Loses: {0:03}".format(self.loses))



