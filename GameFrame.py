import tkinter as tk
from functools import partial
from tkinter import messagebox
from GameEngine import*

class GameFrame(tk.Frame):
    def __init__(self, parent,WIDTH=500,HEIGHT=500):
        super().__init__(parent,bg="gray", width=WIDTH, height=HEIGHT)
        self.pack(padx=20, pady=20)
        self.btns = {}
        
        self.parent = parent
        self.engine = GameEngine(frame=self)

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        for j in range(GRID_Y):
            for i in range(GRID_X):
                cell = tk.Button(self, width=10, height=3,
                command = partial(self.button_click,i,j)
                )
                self.btns[(i,j)] = cell
                cell.grid(row=j,column=i,)
        


    def button_click(self, i,j):

        #gameState is returned upon button_click
        gameState = self.engine.button_click(i, j)

        #signal endgame reset the board
        if(gameState == -1):
            messagebox.showinfo("Bomb", "Dang")
            self.parent.loses.update()
            self.reset_game()
        if(gameState == 1):
            messagebox.showinfo("You won!", "Great")
            self.parent.wins.update()
            self.reset_game()


    def reset_game(self):
        self.engine = GameEngine(frame=self)

        for j in range(GRID_Y):
            for i in range(GRID_X):
                cellDestroy = self.btns.get((i, j))
                cellDestroy.destroy()
                self.btns[(i,j)] = tk.Button(self, width=10, height=3,
                command = partial(self.button_click,i,j)
                )
                self.btns[(i,j)].grid(row=j,column=i,)

