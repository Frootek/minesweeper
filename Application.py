import tkinter as tk
from StartPage import StartPage
from MineSweeper import MineSweeper


class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        super().title("Minesweeper")
        

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True )
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        
        self.frames = {}

        #add StartPage and MineSweeper to container
        frame = StartPage(container,self)
        self.frames["StartPage"] = frame
        frame.grid(row = 0, column=0 , sticky = "nsew")

        frame = MineSweeper(container, self)
        self.frames["MineSweeper"] = frame
        frame.grid(row = 0, column=0 , sticky = "nsew")

        self.show_frame("StartPage")


    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

app = Application()
app.mainloop()





