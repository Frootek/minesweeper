import tkinter as tk
from LabelWidgets import *
from StartPage import StartPage
from GameFrame import GameFrame

HEIGHT  = 700
WIDTH   = 700

class MineSweeper(tk.Frame):
    def __init__(self, parent, controller ):
        tk.Frame.__init__(self,parent)
        self.controller = controller
        self.create_widgets()


    def create_widgets(self):
        self.upper_container = tk.Label(self,bg="gray")
        self.wins = Wins(self.upper_container)
        self.resetScore = CenterBtn(self.upper_container, self)
        self.loses = Loses(self.upper_container)
        self.upper_container.pack(padx=20, pady=10, fill="x")
        self.gameFrame = GameFrame(self)


    

