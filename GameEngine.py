import random
import tkinter as tk
from PIL import Image, ImageTk


GRID_X  = 8
GRID_Y  = 8
NUM_MINES = 20

colors = {
    "COLOR_1" : "#0000cc",
    "COLOR_2" : "#6666ff",
    "COLOR_3" : "#ff00ff",
    "COLOR_4" : "#e6004c",
    "COLOR_5" : "#ff1a1a",
    "COLOR_6" : "#ff0000",
    "COLOR_7" : "#b30000",
    "COLOR_8" : "#660000",
}

class GameEngine():
    def __init__(self, frame):
        super().__init__()
        self.init_game()
        self.gameFrame = frame

    def init_game(self):
        #place NUM_MINES on gamegrid
        self.mines = {}
        self.flags = {}
        self.revealed = {}
        self.firstClick = True
        self.place_mines()
        self.mineImg = ImageTk.PhotoImage(Image.open("images/mine.png").resize((60, 45)))

    
    def place_mines(self):
        i = 0
        while(i < NUM_MINES):
            
            x = random.randint(0,GRID_X-1)
            y = random.randint(0,GRID_Y-1)

            if(self.mines.get((x,y),0) != 0):
                continue
            self.mines[(x,y)] = 1

            i=i+1
            
    def clear_mines(self):
        self.mines.clear()
    

    def  out_of_bounds(self, x, y):
        return (( x < 0 ) or ( y < 0 ) or ( x >= GRID_X ) or (y >= GRID_Y))

    def calc_near_mines(self, x, y):
        if (self.out_of_bounds(x,y)):
            return 0

        near_mines = 0

        for j in range (-1,2):
            for i in range(-1,2):
                if (self.out_of_bounds(i+x,j+y)):
                    continue 
                near_mines+=self.mines.get((i+x,j+y), 0)
        return near_mines

    def reveal_squares(self, x, y):

        #dont do anything if its out of bounds
        if (self.out_of_bounds(x,y)):
            return 

        #if already revealed dont do anything
        if(self.revealed.get((x, y), 0) != 0):
            return
        
        #reveal square
        self.revealed[(x, y)] = 1
        #destroy square
        self.reveal_btn(x,y)

        #if there are mines near stop revealing in this direction
        if(self.calc_near_mines(x,y) != 0):
            return
        
        #left squarebox
        self.reveal_squares(x-1, y-1)
        self.reveal_squares(x-1, y)
        self.reveal_squares(x-1, y+1)

        #right squarebox
        self.reveal_squares(x+1, y-1)
        self.reveal_squares(x+1, y)
        self.reveal_squares(x+1, y+1)

        #middle top and bottom squares
        self.reveal_squares(x, y-1)
        self.reveal_squares(x, y+1)
    

    def calculate_win(self):
        return NUM_MINES == ((GRID_X * GRID_Y)-len(self.revealed))



    #  1 if game is won 
    # 0 if you cgame continues
    #  -1 if game is lost
    def button_click(self, x, y):

        if(self.firstClick):
            self.firstClick = False
            while(self.calc_near_mines(x, y) != 0):
                self.clear_mines()
                self.place_mines()
        

        #if you clicked mine return -1
        if(self.mines.get((x,y), 0 ) != 0):
            self.show_mines()
            return -1
        #if not reveal from that square
        else:
            self.reveal_squares(x, y)
        gameWon = self.calculate_win()
        if(gameWon):
            return 1

        return 0


    def show_mines(self):
        for key in self.mines:
            (x,y) = key 
            mine  =  self.gameFrame.btns[key]
            mine.destroy()

            mineReveal = tk.Label(self.gameFrame, image=self.mineImg,
             bg="#F0F0F0", relief="groove")
            
            mineReveal.grid(row = y, column = x, stick="nesw")




    def reveal_btn(self, x, y):

        numOfBombs = self.calc_near_mines(x, y)

        self.gameFrame.btns[(x, y)].destroy()

        revealedBtn = tk.Label(self.gameFrame,
        bg="#F0F0F0", width=10, height=3, relief="groove")


        if(numOfBombs != 0):
            revealedBtn.config(text="{}".format(numOfBombs), 
            fg=colors.get("COLOR_{}".format(numOfBombs)))
            revealedBtn.config(font=("Arial", 9, "bold"))


        revealedBtn.grid(row=y, column=x, stick="nesw")


        


