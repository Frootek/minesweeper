# Minesweeper

Little project I did while learning Tkinter(pythons module for creating GUI apps).
The goal was to recreate the  popular game "Minesweeper.

## Usage

Clone the repository and run following command 

```bash
python Application.py
```

Note: you need to have python installed on your operating system. If you don't please visit [https://www.python.org/downloads/](https://www.python.org/downloads/).

Have fun!

## The game


![Image1](images/md_example.JPG)

## License
This software is licensed under the [MIT License](LICENSE).

